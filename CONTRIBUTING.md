To contribute to this repository you will need to create an [Eclipse Account](https://accounts.eclipse.org/user/register) and complete and submit an [Eclipse Contributor Agreement (ECA)](https://www.eclipse.org/legal/ECA.php). You can learn more about the contributor agreement in the [FAQ](https://www.eclipse.org/legal/ecafaq.php).


Feel free to [open an issue](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics/-/issues/new) or [submit a merge request](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics/-/merge_requests/new) (see [GitLab documentation on merge request](https://docs.gitlab.com/ee/user/project/merge_requests/index.html) for more information on merge requests).

If you have questions on how to contribute to this repository, please email Tobie Langel (<tobie.langel@eclipse-foundation.org>).
