import TYPE from "./types.js";
import TERMS from "./terms.js";
import { slug, identify } from "./helpers.js";

export default function parse(elements, i18n) {
    const craStruct = [];
    let __BUFFER__ = null;
    let __SECTION__ = null;
    const __DEPTH__ = [];
    const __UNDEPTH__ = [];
    let __ANNEX__ = null;
        
    function addNode(obj) {
        obj.sectionId = __SECTION__;
        if (__ANNEX__) {
            obj.annex = __ANNEX__;
        }
        obj.undepth = [...__UNDEPTH__];
        obj.depth = [...__DEPTH__];
        obj.point = __DEPTH__.slice(1);
        if (obj.type == TYPE.POINT || obj.type == TYPE.ARTICLE || obj.type == TYPE.RECITAL || obj.type == TYPE.ANNEX_SUBSECTION) {
            obj.id = identify(__DEPTH__);
        } else if (obj.type == TYPE.SECTION_SEPARATOR || obj.type == TYPE.ANNEX || obj.type == TYPE.CHAPTER) {
            obj.id = slug(__UNDEPTH__.at(-1));
        }
        if (obj.regex) {
            obj.regex = obj.regex
        }
        obj.TYPE = TYPE;
        craStruct.push(obj)
    }
    
    elements.forEach((element, i) => {

        let text = element.textContent.replace(/\s+/g, " ").trim();

        if (!text) {
            return;
        }

        if (text.indexOf(i18n.MARKERS.PREAMBLE_SECTION) == 0) {
            console.log("Parsing premable…")
            addNode({ type: TYPE.PARAGRAPH, text });
            __SECTION__ = TERMS.PREAMBLE;
            __UNDEPTH__.length = 0;
            __UNDEPTH__.push(TERMS.PREAMBLE);
            return;
        }


        if (text.indexOf(i18n.MARKERS.RECITALS_SECTION) == 0) {
            console.log("Parsing recitals…")
            addNode({ type: TYPE.PARAGRAPH, text });
            __SECTION__ = TERMS.RECITALS;
            __UNDEPTH__.length = 0;
            __UNDEPTH__.push(TERMS.RECITALS);
            addNode({ type: TYPE.SECTION_SEPARATOR, text: i18n.TERMS.RECITALS });
            return;
        }

        if (text.indexOf(i18n.MARKERS.REGULATION_SECTION) == 0) {
            console.log("Parsing regulation…")
            addNode({ type: TYPE.PARAGRAPH, text });
            __SECTION__ = TERMS.REGULATION;
            __UNDEPTH__.length = 0;
            __UNDEPTH__.push(TERMS.REGULATION);
            __DEPTH__.length = 0;
            addNode({ type: TYPE.SECTION_SEPARATOR, text: i18n.TERMS.REGULATION });
            return;
        }
        
        if (text == i18n.MARKERS.ANNEX_SECTION) {
            console.log("Parsing the annexes…")
            __SECTION__ = TERMS.ANNEXES;
            __UNDEPTH__.length = 0;
            __UNDEPTH__.push(TERMS.ANNEXES);
            __DEPTH__.length = 0;
            addNode({ type: TYPE.SECTION_SEPARATOR, text: i18n.TERMS.ANNEXES });
        }
        
        if (element.tagName == "TABLE" && element.className == "inpage_annotation_doc") {
            console.log("Parsing the footnotes…")
            __SECTION__ = TERMS.FOOTNOTES;
            __UNDEPTH__.length = 0;
            __UNDEPTH__.push(TERMS.FOOTNOTES);
            __ANNEX__ = null;
            __DEPTH__.length = 0;
            addNode({ type: TYPE.SECTION_SEPARATOR, text: i18n.TERMS.FOOTNOTES });
            
            element.textContent.split("\n").forEach(line => {
                const text = line.replace(/^\((\d+)\)\s*([^\n]+)$/, "[^$1]: $2");
                addNode({ type: TYPE.FOOTNOTE, text });
            });
            return;
        }
        
        if (__SECTION__ == TERMS.PREAMBLE) {
            addNode({ type: TYPE.PARAGRAPH, text });
        } else if (__SECTION__ == TERMS.RECITALS) {
            const regex = /^\((\d+)\)/;
            const match = text.match(regex);
            if (match) {
                __DEPTH__[0] = "Recital " + match[1];
                addNode({ type: TYPE.RECITAL, text, regex, citation: match[0] });
            } else {
                addNode({ type: TYPE.PARAGRAPH, text });
            }
        } else if (__SECTION__ == TERMS.REGULATION) {
            if (text.startsWith(i18n.MARKERS.CHAPTER)) {
                __BUFFER__ = { type: TYPE.CHAPTER, text };
                __UNDEPTH__.length = 1;
                __UNDEPTH__.push(text);
            } else if (text.startsWith(i18n.MARKERS.ARTICLE)) {
                __DEPTH__.length = 0;
                __DEPTH__[0] = text;
                __BUFFER__ = { type: TYPE.ARTICLE, text };
            } else if (__BUFFER__) {
                addNode({ type: __BUFFER__.type, text: __BUFFER__.text + i18n.TERMS.SEPARATOR + text })
                __BUFFER__ = null;
            } else {
                // Article 43 1.
                // ["Article 12", "1"]
                let regex = /^(\d+)\./;
                let match = text.match(regex);
                if (match == null) {
                    // Article 43 (1)
                    // ["Article 12", "1"]
                    regex = /^\((\d+)\)/;
                    match = text.match(regex);
                }
                if (match) {
                    __DEPTH__.length = 1;
                    __DEPTH__[1] = match[1];
                    addNode({ type: TYPE.POINT, text, regex, citation: match[0] });
                } else {
                    // Article 43 1. (a) (i)
                    // ["Article 43", "1", "a", "i"]
                    const regex = /^\(([ivx]+)\)/;
                    const match = text.match(regex);
                    if (match) {
                        __DEPTH__[3] = match[1];
                        addNode({ type: TYPE.POINT, text, regex, citation: match[0] });
                    } else {
                        // ASSUMPTION: letters i and beyond are only used for roman notation
                        const regex = /^\(([a-h])\)/; 
                        const match = text.match(regex);
                        if (match) {
                            if ((/\d+/).test(__DEPTH__[1])) {
                                // Article 43 1. (a)
                                // ["Article 43", "1", "a"]
                                __DEPTH__.length = 2;
                                __DEPTH__[2] = match[1];
                                addNode({ type: TYPE.POINT, text, regex, citation: match[0] });
                            } else {
                                // Article 43 (a)
                                // ["Article 43", "a"]
                                __DEPTH__.length = 1;
                                __DEPTH__[1] = match[1];
                                addNode({ type: TYPE.POINT, text, regex, citation: match[0] });
                            }
                        } else {
                            __DEPTH__.length = 2;
                            addNode({ type: TYPE.PARAGRAPH, text });
                        }
                    }
                }
            }
        } else if (__SECTION__ == TERMS.ANNEXES) {
            if (text.startsWith(i18n.MARKERS.ANNEX)) {
                __DEPTH__.length = 0;
                __DEPTH__[0] = text;
                __ANNEX__ = text;
                __UNDEPTH__.length = 1;
                __UNDEPTH__.push(text);
                __BUFFER__ = { type: TYPE.ANNEX, text };
            } else if (text.startsWith(i18n.MARKERS.LAST_ANNEX)) {
                __DEPTH__.length = 0;
                __DEPTH__[0] = text;
                __ANNEX__ = text;
                __UNDEPTH__.length = 1;
                __UNDEPTH__.push(text);
                addNode({ type: TYPE.ANNEX, text: text });
            } else if (text.startsWith(i18n.MARKERS.CLASS) || text.startsWith(i18n.MARKERS.PART)) {
                __DEPTH__.length = 0;
                const part = text.match(/\w+\s[IVX]+/)[0];
                __DEPTH__[0] = __ANNEX__ + ", " + part;
                __UNDEPTH__.length = 2;
                __UNDEPTH__.push(part);
                addNode({ type: TYPE.ANNEX_SUBSECTION, text: text.replace(/^(\w+\s[IVX]+)\s+/, "$1" + i18n.TERMS.SEPARATOR) });
            } else if (__BUFFER__) {
                addNode({ type: __BUFFER__.type, text: __BUFFER__.text + i18n.TERMS.SEPARATOR + text });
                __BUFFER__ = null;
            } else {
                // Annex II 1.
                let regex = /^(\d+)\.(?:(\d+))?\.?/;
                let match = text.match(regex);
                if (match == null) {
                    // Annex II (1)
                    regex = /^\((\d+)\)/;
                    match = text.match(regex);
                }
                if (match) {
                    __DEPTH__.length = 1;
                    __DEPTH__[1] = match[1];
                    if (match[2]) {
                        __DEPTH__[2] = match[2];
                    }
                    addNode({ type: TYPE.POINT, text, regex, citation: match[0] });
                } else {
                    // Annex II 1. (a)
                    const regex = /^\((\w)\)/; 
                    const match = text.match(regex);
                    if (match) {
                        __DEPTH__[2] = match[1];
                        addNode({ type: TYPE.POINT, text, regex, citation: match[0] });
                    } else {
                        // Normalize hyphens used in lists
                        text = text.replace(/^—\s/g, "- ");
                        if (text.startsWith("- ")) {
                            addNode({ type: TYPE.LIST_ITEM, text: text.replace(/^-\s/g, "") });
                        } else {
                            addNode({ type: TYPE.PARAGRAPH, text });
                        }
                    }
                }
            }
        }
    });
    return craStruct;
}