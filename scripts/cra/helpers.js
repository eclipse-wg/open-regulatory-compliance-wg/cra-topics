function slug(str) {
    return str.toLowerCase().trim().replace(/\W+/g, "-");
}

function identify(depth) {
    return depth.map(slug).join("-");
}

export { identify, slug };