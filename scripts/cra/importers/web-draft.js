import { JSDOM } from "jsdom";
import { replaceBreaks, cleanupTxt, cleanup } from "./import-helpers.js";

async function request(url) {
    const response = await fetch(url);
    const html = await response.text();
    return html;
}

function extractFootnotes(window) {
    const table = [...window.document.querySelectorAll("table.inpage_annotation_doc")].at(-1);
    cleanup(table.childNodes);
    const footnotes = table.textContent.trim().split("\n").map(line => {
        const match = line.trim().match(/^\((\d+)\)\s*([^\n]+)$/);
        return { number: match[1], text: match[2] };
    });
    
    table.remove();
    return footnotes;
}

async function webImporter(path) {

    const html = await request(path);
    
    // Get the table cell that contains the CRA text
    const window = new JSDOM(html).window;
    const td = window.document.querySelectorAll("tr.contents:last-of-type > td")[1];
    
    // Get the table row that contains part of the preamble
    const doc_title = Array.from(window.document.querySelectorAll("tr.doc_title")).at(-1).querySelectorAll("td")[0];
    
    cleanupTxt(doc_title.childNodes, window);
    cleanupTxt(td.childNodes, window);
    
    const footnotes = extractFootnotes(window);
    const elements = [doc_title, ...Array.from(td.children)];
    cleanup(elements);
    
    return { elements, footnotes };
}

export default webImporter;