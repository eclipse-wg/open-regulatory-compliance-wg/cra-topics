
function cleanupTxt(nodes, window) {
    Array.from(nodes).forEach(node => {
        if (node.nodeType == window.Node.TEXT_NODE) {
            node.nodeValue = node.nodeValue.replaceAll("▌", "").replaceAll("&nbsp;", " ").replace(/^\s*\*/, "\\*");
        } else if (node.nodeType == window.Node.ELEMENT_NODE) {
            cleanupTxt(node.childNodes, window);
        }
    })
}

function cleanupFormating(nodes, window) {
    Array.from(nodes).forEach(node => {
        cleanupFormating(node.childNodes);
        if (node.tagName == "STRONG" || node.tagName == "EM") {
            node.replaceWith(...node.childNodes);
        }
    })
}

function replaceBreaks(node, window) {
    if (node.nodeType != window.Node.ELEMENT_NODE) {
        return;
    }
    if (node.tagName != "P") {
        Array.from(node.childNodes).forEach(child => replaceBreaks(child, window));
        return;
    }
    
    let newPara = null;
    // we're in a paragraph!
    Array.from(node.childNodes).forEach(child => {
        if (child.tagName == "BR") {
            newPara = node.ownerDocument.createElement("P");
            node.after(newPara);
            child.remove();
        } else if (newPara) {
            newPara.appendChild(child);
        }
    })
}


function cleanup(children) {
    Array.from(children).forEach(element => {
        cleanup(element.children);
        if (element.tagName == "SPAN") {
            if (element.className == "sup") {
                let text = element.textContent.trim();
                element.replaceWith((text == "\\*" || text == "+") ? text : `[^${text.replace(/\D/g, "")}]`);
            } else {
                element.replaceWith(...element.childNodes);
            }
        } else if (element.tagName == "A") {
            const href = element.getAttribute("href");
            if (!href || href.startsWith("#")) {
                element.replaceWith(...element.childNodes);
            } else {
                element.replaceWith(`[${element.textContent}](${href})`);
            }
        }
    });
}


export { cleanup, replaceBreaks, cleanupTxt, cleanupFormating };
