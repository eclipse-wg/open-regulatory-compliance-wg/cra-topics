import { JSDOM } from "jsdom";
import mammoth from "mammoth";
import { replaceBreaks, cleanupTxt, cleanupFormating } from "./import-helpers.js";

function cleanupDocx(children) {
    Array.from(children).forEach(element => {
        cleanupDocx(element.children);
        if (element.tagName == "SUP") {
            const a = element.querySelector("a");
            if (a) {
                let text = a.textContent.trim();
                element.replaceWith((text == "\\*" || text == "+") ? text : `[^${text.replace(/\D/g, "")}]`);
            } else {
                element.replaceWith(...element.childNodes);
            }
        }
    });
}

async function docxImporter(path) {
    var options = {
        styleMap: [
            "DocRef => div"
        ],
        convertImage: mammoth.images.imgElement(function(image) {
        return image.readAsBase64String().then(function(imageBuffer) {
            return { src: "" };
        });
    }),
        includeDefaultStyleMap: false
        
    };
    const results = await mammoth.convertToHtml({ path }, options)
    const html = results.value;
    const window = new JSDOM(html).window;
    const footnotes = extractFootnotes(window);
    cleanupTxt(window.document.childNodes, window);
    cleanupFormating(window.document.body.childNodes, window);
    replaceBreaks(window.document.documentElement, window);
    cleanupDocx(window.document.body.childNodes);
    return {
        elements: [...window.document.body.childNodes],
        footnotes
    };
}

function toMDAnchor(element) {
    let anchors = element.querySelectorAll("a");
    [...anchors].forEach(anchor => {
        if (anchor.getAttribute("href").startsWith("#")) {
            anchor.remove();
        } else {
            anchor.replaceWith(`[${anchor.textContent}](${anchor.getAttribute("href")})`)
        }
    });
    return element;
}
 
function extractFootnotes(window) {
    const element = window.document.querySelector("#footnote-1").parentNode;
    const footnotes = [...element.children].map(li => {
        return { number: li.id.split("-").at(-1), text: toMDAnchor(li).textContent.trim() };
    });
    
    element.remove();
    return footnotes;
}

export default docxImporter;
