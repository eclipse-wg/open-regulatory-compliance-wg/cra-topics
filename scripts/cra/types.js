const TYPE = {
    SECTION_SEPARATOR: "section-separator",
    CHAPTER: "chapter",
    ARTICLE: "article",
    RECITAL: "recital",
    ANNEX: "annex",
    ANNEX_SUBSECTION: "annex-subsection",
    POINT: "point",
    PARAGRAPH: "paragraph",
    LIST_ITEM: "list-item",
    FOOTNOTE: "footnote"
};

export default TYPE;