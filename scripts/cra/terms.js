
const TERMS = {
    PREAMBLE: "Preamble",
    RECITALS: "Recitals",
    REGULATION: "Regulation",
    ANNEXES: "Annexes",
    FOOTNOTES: "Notes",
    SEPARATOR: " - "
}

export default TERMS;