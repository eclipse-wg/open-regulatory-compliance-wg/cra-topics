import TYPE from "../types.js";
import { identify } from "../helpers.js";

function createTOC(struct) {
    const output = ["<ul>"];
    
    function anchor(element) {
        return `<a href="#${ element.id }">${ element.text }</a>`;
    }
    let prevDepth = -1;
    const TAB = "   ";
    struct.forEach(element => {
            if (element.type == TYPE.SECTION_SEPARATOR) {
                if (prevDepth == 2) {
                    output.push(TAB.repeat(4) + "</ul>");
                }
                if (prevDepth >= 1) {
                    output.push(TAB.repeat(3) + "</li>");
                    output.push(TAB.repeat(2) + "</ul>");
                }
                if (prevDepth >= 0) {
                    output.push(TAB + "</li>");
                }
                output.push(TAB + "<li>");
                output.push(TAB.repeat(2) + anchor(element));
                prevDepth == 0;
            } else if(element.type == TYPE.CHAPTER || element.type == TYPE.ANNEX) {
                if (prevDepth == 0) {
                    output.push(TAB.repeat(2) + "<ul>");
                } else if (prevDepth == 1) {
                    output.push(TAB.repeat(3) + "</li>");
                } else if (prevDepth == 2) {
                    output.push(TAB.repeat(4) + "</ul>");
                    output.push(TAB.repeat(3) + "</li>");
                }
                output.push(TAB.repeat(3) + "<li>");
                output.push(TAB.repeat(4) + anchor(element));
                prevDepth = 1;
            } else if (element.type == TYPE.ARTICLE || element.type == TYPE.ANNEX_SUBSECTION) {
                if (prevDepth == 1) {
                    output.push(TAB.repeat(4) + "<ul>");
                }
                output.push(TAB.repeat(5) + `<li>${ anchor(element) }</li>`);
                prevDepth = 2;
            }
    });
    output.push("</ul>")
    return output.join("\n")
}

function addAnchor(node) {
    return node.text.replace(node.regex, anchor(node));
}

function createTitle(depth) {
    let str = "";
    const part1 = [];
    const part2 = [];
    depth.forEach(p => {
        if (/\s+/.test(p)) {
            part1.push(p)
        } else {
            part2.push(`(${p})`);
        }
    });
    return part1.join(" ") + part2.join("");
}

function anchor(node) {
    const title = createTitle(node.depth);
    const id = identify(node.depth);
    return `<span class="citation"><a title="${ title }" id="${ id }" href="#${ id }">${ node.citation }</a></span>`;
}



function mdPrefix(node) {
    let count = node.point.length;
    let prefix = "";
    if (node.type == node.TYPE.LIST_ITEM || node.type == node.TYPE.PARAGRAPH) {
        count++;
    }
    if (count > 1) {
        prefix = "- ";
    }
    if (node.type == node.TYPE.PARAGRAPH) {
        prefix = "";
    }

    if (count > 2) {
        prefix = "  ".repeat(count - 2) + prefix;
    }
    return prefix;
}

function createMarkdownNotice({ title, source, date }) {
    const THIS_SCRIPT_URL = "https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics/-/blob/main/scripts/cra.js";
    return `<p><em>This is a <a href="${ THIS_SCRIPT_URL }">programatically generated</a> version of ${ title } retrieved from ${ source } on ${ date }. It has no legal standing whatsoever and is made available only for internal reference of the ORC WG.</em><p>`;
}

function renderFootnote(footnote) {
    const num = footnote.number;
    const backref = `<a class="footnote-backref" aria-label="Back to reference ${num}" href="#footnote-ref-${num}">↑</a>`;
    return `<li id="footnote-${num}"><sup>${num}</sup> ${footnote.text} ${backref}</li>`;
}

function renderFootnotes(footnotes) {
    return [
        "<ol id=footnotes>",
        footnotes.map(n => "    " + renderFootnote(n)).join("\n"),
        "</ol>"
    ].join("\n");
}

function wrap(tag, content) {
    return `<${tag}>${content}</${tag}>`
}

function render({ title, source, date, content, toc, footnotes }) {
    const notice = createMarkdownNotice({ title, source, date });
    toc = createTOC(content);
    let inList = false;
    let prevDepth = 0;
    content = content.map(node => {
        let txt = "";

        if (inList && node.type != TYPE.LIST_ITEM) {
            txt += "</ul>\n\n";
            inList = false;
        }
        
        const currDepth = node.depth.length;
        while (currDepth > prevDepth) {
            txt += "<section>\n\n";
            prevDepth++;
        }
        while (currDepth < prevDepth) {
            txt += "</section>\n\n";
            prevDepth--;
        }

        switch (node.type) {
            case TYPE.SECTION_SEPARATOR:
                txt += wrap("h2", node.text);
                break;
            case TYPE.CHAPTER:
                txt += wrap("h3", node.text);
                break;
            case TYPE.ARTICLE:
                txt += wrap("h4", node.text);
                break;
            case TYPE.RECITAL:
                txt += wrap("p", addAnchor(node));
                break;
            case TYPE.ANNEX:
                txt += wrap("h3", node.text);
                break;
            case TYPE.ANNEX_SUBSECTION:
                txt += wrap("h4", node.text);
                break;
            case TYPE.POINT:
                txt += wrap("p", addAnchor(node));
                break;
            case TYPE.PARAGRAPH:
                txt += wrap("p", node.text);
                break;
            case TYPE.LIST_ITEM:
                if (!inList) {
                    txt += "<ul>\n\n";
                }
                inList = true;
                txt += "    " + wrap("li", node.text);
                break;
        }
        return txt;
    }).join("\n\n");
    return ["<style>section > section { margin-left: 50px; }</style>", notice, "<h1>" + title + "</h1>", toc, content, "<hr />", renderFootnotes(footnotes)].join("\n\n") + "\n";
}

export default render;
