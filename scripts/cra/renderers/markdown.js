import TYPE from "../types.js";
import { identify } from "../helpers.js";

function addAnchor(node) {
    return node.text.replace(node.regex, anchor(node));
}

function createTitle(depth) {
    let str = "";
    const part1 = [];
    const part2 = [];
    depth.forEach(p => {
        if (/\s+/.test(p)) {
            part1.push(p)
        } else {
            part2.push(`(${p})`);
        }
    });
    return part1.join(" ") + part2.join("");
}

function anchor(node) {
    const title = createTitle(node.depth);
    const id = "cra-" + identify(node.depth);
    return `<strong><a title="${ title }" name="${ id }" href="#${ id }">${ node.citation }</a></strong>`;
}

function mdPrefix(node) {
    let count = node.point.length;
    let prefix = "";
    if (node.type == node.TYPE.LIST_ITEM || node.type == node.TYPE.PARAGRAPH) {
        count++;
    }
    if (count > 1) {
        prefix = "- ";
    }
    if (node.type == node.TYPE.PARAGRAPH) {
        prefix = "";
    }

    if (count > 2) {
        prefix = "  ".repeat(count - 2) + prefix;
    }
    return prefix;
}

function createMarkdownNotice({ title, source, date }) {
    const THIS_SCRIPT_URL = "https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics/-/blob/main/scripts/cra.js";
    return `_This is a [programatically generated](${ THIS_SCRIPT_URL }) version of ${ title } retrieved from ${ source } on ${ date }. It has no legal standing whatsoever and is made available only for internal reference of the ORC WG._`;
}

function markdownRenderer({ title, source, date, content, toc, footnotes }) {
    const notice = createMarkdownNotice({ title, source, date });
    content = content.map(node => {
        let txt;
        switch (node.type) {
            case TYPE.SECTION_SEPARATOR:
                txt = "## " + node.text;
                break;
            case TYPE.CHAPTER:
                txt = "### " + node.text;
                break;
            case TYPE.ARTICLE:
                txt = "#### " + node.text;
                break;
            case TYPE.RECITAL:
                txt = mdPrefix(node) + addAnchor(node);
                break;
            case TYPE.ANNEX:
                txt = "### " + node.text;
                break;
            case TYPE.ANNEX_SUBSECTION:
                txt = "#### " + node.text;
                break;
            case TYPE.POINT:
                txt = mdPrefix(node) + addAnchor(node);
                break;
            case TYPE.PARAGRAPH:
                txt = mdPrefix(node) + node.text;
                break;
            case TYPE.LIST_ITEM:
                txt = mdPrefix(node) + node.text;
                break;
        }
        return txt;
    }).join("\n\n");
    footnotes = footnotes.map(n => `[^${n.number}]: ${n.text}`).join("\n")
    return [notice, "# " + title, "[[_TOC_]]", content, "---", footnotes].join("\n\n") + "\n";
}

export default markdownRenderer;
