import { JSDOM } from "jsdom";
import { writeFile } from "node:fs/promises";

import TYPE from "./cra/types.js";
import TERMS from "./cra/terms.js";
import parse from "./cra/parser.js";
import { identify } from "./cra/helpers.js";
import markdownRenderer from "./cra/renderers/markdown.js";
import htmlRenderer from "./cra/renderers/html.js";
import docxImporter from "./cra/importers/docx.js";
import webDraftImporter from "./cra/importers/web-draft.js";

const EN = {
    LANG: "en-GB", 
    PATH: "./TA-9-2024-0130-FNL-COR01_EN.docx",
    URL: "https://www.europarl.europa.eu/doceo/document/TA-9-2024-0130_EN.html",
    TITLE: "The Cyber Resilience Act (CRA)",
    MARKERS: {
        PREAMBLE_SECTION: "Position of the European Parliament",
        RECITALS_SECTION: "Whereas:",
        REGULATION_SECTION: "HAVE ADOPTED THIS REGULATION:",
        ANNEX_SECTION: "Annex I",
        CHAPTER: "CHAPTER",
        ARTICLE: "Article",
        LAST_ANNEX: "ANNEX",
        ANNEX: "Annex",
        PART: "Part",
        CLASS: "Class"
    },
    TERMS
};

const FR = {
    LANG: "fr-FR", 
    URL: "https://www.europarl.europa.eu/doceo/document/TA-9-2024-0130_FR.html",
    TITLE: "Législation sur la Cyberrésilience",
    MARKERS: {
        RECITALS_SECTION: "considérant ce qui suit:",
        REGULATION_SECTION: "ONT ADOPTÉ LE PRÉSENT RÈGLEMENT:",
        ANNEX_SECTION: "Annexe I",
        CHAPTER: "CHAPITRE",
        ARTICLE: "Article",
        LAST_ANNEX: "ANNEXE",
        ANNEX: "Annexe",
        PART: "Partie",
        CLASS: "Classe"
    },
    TERMS: {
        PREAMBLE: "Préambule",
        RECITALS: "Considérations",
        REGULATION: "Règlement",
        ANNEXES: "Annexes",
        FOOTNOTES: "Notes de bas de page"
    }
};

const TARGET = "../cra.md";

export default async function main(i18n, target) {
    i18n = i18n || EN;
    const URL = i18n.URL;
    const TITLE = i18n.TITLE;
    const DATE = new Intl.DateTimeFormat(i18n.LANG, {
        dateStyle: 'full',
        timeStyle: 'long',
    }).format(new Date());
    
    console.log("Start importing.")
    
    const source = i18n.PATH ? i18n.PATH : i18n.URL;
    let src;
    if (i18n.PATH) {
        console.log("Importing docx document from " + source)
        src = await docxImporter(source);
    } else {
        console.log("Importing HTML page from " + source)
        src = await webDraftImporter(source);
    }
    
    console.log("Done importing.")
    console.log("Start parsing.")
    
    const craStruct = parse(src.elements, i18n);
    
    console.log("Done parsing.")
    console.log("Now writing to file " + target);

    await writeFile(target, markdownRenderer({
        title: TITLE,
        source,
        date: DATE,
        content: craStruct,
        footnotes: src.footnotes
    }));
    console.log("Completed.");
}

await main(EN, TARGET);

EN.PATH ="./TA-9-2024-0130_EN.docx";
main(EN, "../cra-2024-03-12.md");





