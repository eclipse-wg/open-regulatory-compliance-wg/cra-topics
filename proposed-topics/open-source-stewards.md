# Discussion topic: Open Source Stewards

**Topic status:** proposed

**Matrix Channel:** TBD

**Facilitator**: TBD

## Participants

* [NAME], [AFFILIATION] (GITLAB_HANDLE)

## Topic description

The [CRA](../CRA.md) defines a new category of actors in the open source ecosystem: _Open Source Stewards_. _Open Source Stewards_ are roughly mapped after open source foundations. When it comes to the CRA, _Open Source Stewards_ are subject to a lighter compliance regime.

The purpose of the discussions is for representatives of foundations to discuss how the CRA impacts their organisation, how they can support their communities and their members, what activities are permitted within the bounds of the _Open Source Stewards_ regime, etc. 

## Output

Output of these discussions might include:

- defining what kind of support material _Open Source Stewards_ would need to comply to the CRA
- outlining, and/or authoring such material
- providing input to other discussion topics and specification proposals

## Working mode

Regular meetings on Zoom. All meetings are public.

Meeting notes are shared publicly in this repository.



