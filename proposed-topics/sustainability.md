# Discussion topic: explore the CRA impact's on open source sustainability

**Topic status:** proposed

**Facilitator(s)**: TBD

**Staff contact**: Tobie Langel (<tobie.langel@eclipse-foundation.org>)

## Topic description

The purpose of this discussion topic is to explore and document the different aspects, both positive and negative, in which the CRA might impact open source sustainability and to identify risks and opportunities. Discussion topics include:

- Can providing [security attestations][attest] be a source of revenue for open source stewards and maintainers?
- Who can create [security attestations][attest]?
- Is there are risk of a race to the bottom if anyone can create [security attestations][attest]?
- Does the CRA offer additional revenue opportunities for [open source stewards][steward] and maintainers?
- What is the tipping point when [open source stewards][steward] and maintainers become manufacturers?  

## Output

- Input to the CRA FAQ
- Documentation on the risks and benefits of the CRA for open source sustainability

## Resources

- [Mailing List](https://accounts.eclipse.org/mailing-list/open-regulatory-compliance) ([Archive](https://www.eclipse.org/lists/open-regulatory-compliance/maillist.html))
- [Community Calendar](https://calendar.google.com/calendar/u/0/embed?src=c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea@group.calendar.google.com) ([iCal format](https://calendar.google.com/calendar/ical/c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea%40group.calendar.google.com/public/basic.ics)) 

## Working mode

- Ad hoc zoom calls as needed.
- Coordination in the Working Group’s [mailing list](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg#getting-involved).
- Meeting notes, collected community feedback, and all other artifacts are linked from or versioned in this repository.

[attest]: ../cra.md#article-25---security-attestation-of-free-and-open-source-software
[steward]: ../cra.md#article-24---obligations-of-open-source-software-stewards