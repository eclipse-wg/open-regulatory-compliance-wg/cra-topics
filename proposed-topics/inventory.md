# Discussion topic: Inventory of resources relevant to the horizontal CRA standards

**Topic status:** proposed

**Facilitator(s)**: TBD

**Staff contact**: Mikael Barbero (<mikael.barbero@eclipse-foundation.org>)

## Topic description

The purpose of this topic is to start an inventory of resources relevant to the horizontal standards as defined in the European Commission's [Draft standardisation request][request] (see [summary tables][standards]):

1. [Horizontal standards due in August 2026][M1] (milestone 1)
2. [Horizontal standards due in September 2027][M2] (milestone 2)

Resources include existing standards, guidelines, best practices, etc., notably:

- Standards for identifiers (CPEs), version numbers, end-of-live status & dates,
referencing (transitive) dependencies, etc
- Standards for releases, recall, EOL,
- Secure by design
- OSS Supply Chain security
- Vulnerability handling
- Etc.

To match the standardisation request timeline, **initial focus should be on resources related to [milestone 1 horizontal standards due in August 2026][M1]**, as the standardisation process for those two standards is impending.

_Note: [vertical standards][vert] are addressed in the discussion topic on [critical and important products](../topics/prod-defs)._

## Output

Output of these discussions may include:

- Inventory of related resources
- Project proposals for [Eclipse Foundation specifications](https://www.eclipse.org/projects/efsp/).

## Resources

- European Commission's [Draft standardisation request][request] (PDF)
- [Standards request summary tables][standards]
- [Mailing List](https://accounts.eclipse.org/mailing-list/open-regulatory-compliance) ([Archive](https://www.eclipse.org/lists/open-regulatory-compliance/maillist.html))
- [Community Calendar](https://calendar.google.com/calendar/u/0/embed?src=c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea@group.calendar.google.com) ([iCal format](https://calendar.google.com/calendar/ical/c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea%40group.calendar.google.com/public/basic.ics)) 

## Working mode

- Ad hoc zoom calls as needed.
- Coordination in the Working Group’s [mailing list](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg#getting-involved).
- Meeting notes, collected community feedback, and all other artifacts are linked from or versioned in this repository.

[request]: https://ec.europa.eu/docsroom/documents/58974
[standards]: ../standards.md
[vert]: ../standards.md#vertical-standards-due-aug-2026
[M1]: ../standards.md#horizontal-standards-due-aug-2026
[M2]: ../standards.md#horizontal-standards-due-sept-2027
