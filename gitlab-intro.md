# Collaborating using GitLab

## Setting up an Eclipse account

Go to: https://accounts.eclipse.org/user/register

## Requesting group membership

Go to https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg and request access by clicking on the three little dots in the top right corner.

Once you've been granted membership, you can safely dismiss both notifications at the top.

## GitLab

### Understanding the structure of GitLab's working groups 

1. GitLab group and subgroups
2. GitLab projects

### GitLab vs. Git

Git is a versioning system with a command line interface.

GitLab is a complete solution for project management including issue tracker, CI/CD, etc.

### GitLab vs. GitHub

1. Fairly similar tool
2. Practical main differences for us:
    - Open source
    - Can be self-hosted
    - pull requests are called merge requests
    - ability to organise groups of projects together (using subgroups)

### What parts of GitLab are actually relevant to spec work?

We only use a tiny subset:

- issue tracker
- versioning markdown files

## Markdown

1. Format [originally invented](https://daringfireball.net/projects/markdown/) by John Gruber
2. Designed to be easy to author structured, HTML-compatible content in plain text
3. Inspired by email formating
4. Lots of dialects
5. Worth looking into [GitLab Flavored Markdown (GLFM)](https://docs.gitlab.com/ee/user/markdown.html)

