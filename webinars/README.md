# Webinar Series on CRA OSS Implementation

1. [How to read the CRA: _Identifying the key parts of the CRA for effective compliance_](#session-1---how-to-read-the-cra-identifying-the-key-parts-of-the-cra-for-effective-compliance)
2. [The CRA obligation: _Identifying the relevant obligations for the OSS community_](#session-2---the-cra-obligation-identifying-the-relevant-obligations-for-the-oss-community)
3. [CRA Standards making: _Understanding key standards and their production timeline_](#session-3---cra-standards-making-understanding-key-standards-and-their-production-timeline)
4. [CRA OSS implementation: _Guidelines, attestations and other key documents_](#session-4---cra-oss-implementation-guidelines-attestations-and-other-key-documents)

👉 [YouTube Playlist](https://www.youtube.com/playlist?list=PLy7t4z5SYNaTRa8iZTcLbal32frbLCwYq)

## Session 1 - How to read the CRA: _Identifying the key parts of the CRA for effective compliance_

![Social card for the session on reading the CRA](cra-webinar-2024-07-15.png)

**Date:** July 15, 2024 at 5pm CEST

**Speaker:** Enzo Ribagnac, Associate Director for European Policy, Eclipse Foundation

**Recording:** https://www.youtube.com/watch?v=IAjHBpwEE_s

**Slides:** [View on GitLab](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics/-/blob/main/webinars/cra-webinar-1-how-to-read-the-cra.pdf?ref_type=heads) | [Download PDF](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics/-/raw/main/webinars/cra-webinar-1-how-to-read-the-cra.pdf?ref_type=heads&inline=false) 



## Session 2 - The CRA obligation: _Identifying the relevant obligations for the OSS community_

![Social card for the session on identifying the relevant obligations the open source community](cra-webinar-2024-07-22.png)
**Date:** July 22,  2024 at 11am CEST

**Speaker:** Benjamin Bögel, Head of Sector for Product Security and Certification Policy at the European Commission

**Recording:** https://www.youtube.com/watch?v=oVTO6s7eMLk

**Slides:** [View on GitLab](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics/-/blob/main/webinars/cra-webinar-2-cra-obligations.pdf?ref_type=heads) | [Download PDF](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics/-/raw/main/webinars/cra-webinar-2-cra-obligations.pdf?ref_type=heads&inline=false) 

## Session 3 - CRA Standards making: _Understanding key standards and their production timeline_

![Social card for the session on understanding key standards for the CRA and their production timeline](cra-webinar-2024-07-29.png)

**Date:** July 29, 2024 at 5pm CEST

**Speaker:** Filipe Jones Mourão, Policy Officer, DG CNECT, European Commission

**Recording:** TBA

**Slides:** TBA

## Session 4 - CRA OSS implementation: _Guidelines, attestations and other key documents_
 
**Date:** September 2, 2024 at 4pm CEST

**Speaker:** TBA

**Recording:** TBA

**Slides:** TBA

