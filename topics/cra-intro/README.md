# Discussion topic: Introduction to the Cyber Resilience Act

**Topic status:** active

**Matrix Channel:** TBD

**Facilitator**: TBD

**Staff contacts**: Enzo Ribagnac (<enzo.ribagnac@eclipse-foundation.org>), Tobie Langel (<tobie.langel@eclipse-foundation.org>)

## Topic description

Do the 300+ pages of the [Cyber Resilience Act (CRA)][cra] feel daunting? Are you struggling to know where to start? Do you have trouble with the terminology? Do you wonder where the standardisation effort fits? Are you confused about what parts of the CRA impact open source?

Don't worry; we've all been there. This discussion topic is here to help you.

The purpose of this discussion is to accelerate the understanding of the CRA across the community. We'll have high level presentations (which we'll record) and regular meetings to make sure everyone's on the same page as quickly as possible.

## Output

Output of these discussions might include:

- A guide to reading the CRA
- Educational material
- Additional resources

## Resources

- [Mailing List](https://accounts.eclipse.org/mailing-list/open-regulatory-compliance) ([Archive](https://www.eclipse.org/lists/open-regulatory-compliance/maillist.html))
- [Community Calendar](https://calendar.google.com/calendar/u/0/embed?src=c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea@group.calendar.google.com) ([iCal format](https://calendar.google.com/calendar/ical/c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea%40group.calendar.google.com/public/basic.ics))
- [CRA resources](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics#cra-resources) - links to the CRA text itself, the standardisation request, etc.

## Working mode

Regular meetings on Zoom. All meetings are public.

Meeting notes are shared publicly in this repository.

[cra]: https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics#what-is-the-cra
