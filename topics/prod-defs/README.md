# Discussion topic: Definition of Critical and Important Product in the CRA

**Topic status:** active

**Facilitator(s)**: TBD

**Staff contact**: Tobie Langel (<tobie.langel@eclipse-foundation.org>)

## Topic description

The European Commission is asking for technical input on the definition of Critical and Important product as defined in Annex III and IV of the [Cyber Resilience Act (CRA)][cra].

The purpose of the discussions is to collect and organize community feedback on the definition of Critical and Important product and share those with the European Commission.

## Output

Output of these discussions include:

- Meeting notes from the consultations organized by the European Commission.
- Organized [technical input][definitions] collected from the community and ready to be shared with the European Commission.
- [Coordination with other groups](#liaisons) also providing technical input to the European Commission.

## Resources

- [Registration form to the European Commission CRA consultation][registration form]
- [Coordination spreadsheet for 25 June - 11 July 2024 consultations][coordination spreadsheet]
- [Mailing List](https://accounts.eclipse.org/mailing-list/open-regulatory-compliance) ([Archive](https://www.eclipse.org/lists/open-regulatory-compliance/maillist.html))
- [Community Calendar](https://calendar.google.com/calendar/u/0/embed?src=c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea@group.calendar.google.com) ([iCal format](https://calendar.google.com/calendar/ical/c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea%40group.calendar.google.com/public/basic.ics)) - lists all CRA consultations
- [Dedicated Matrix channel](https://chat.eclipse.org/#/room/%23open-regulatory-compliance-cra-consultations:matrix.eclipse.org) - for quick coordination and as a backchannel during calls.

## Liaisons

### OpenForum Europe (OFE)

**Rapporteur:** TBD

#### Role of the Rapporteur

- Update OFE community on the content of the discussions in the European Commission CRA Consultations during OFE’s weekly CRA/Open Source Task Force call.
- Gather input from the OFE community and where appropriate integrate it into relevant outputs. 

## Working mode

- Ad hoc zoom calls as needed.
- Coordination in the Working Group’s [mailing list](https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg#getting-involved) and through the [coordination spreadsheet][].
- Meeting notes, collected community feedback, and all other artifacts are linked from or versioned in this repository.

### Joining the European Commission CRA Consultations

1. Register throught the European Commission's [registration form][].
2. Add your name to the [coordination spreadsheet][] for the meetings you're planning to attend.
3. Add key points you're planning to bring to the discussion in the relevant scratchpad linked from the [coordination spreadsheet][] (e.g. [scratchpad for the consultation on browsers](https://docs.google.com/document/d/1YI7bcsrQkN5d0VOBYWamf3_vwH-7EzLTDj6kf3ECyNo/edit#heading=h.j7zymcpe7p3d)).
4. If you're familiar with GitLab, move the finalized notes to GitLab after the call.

[registration form]: https://ec.europa.eu/eusurvey/runner/CRAconsultations
[definitions]: ./definitions.md
[coordination spreadsheet]: https://docs.google.com/spreadsheets/d/1tVEd8A_Bk3k-QBS8CoiHxurYy_IvqcVmL5M2dgayuUg/edit?gid=0#gid=0
[cra]: https://gitlab.eclipse.org/eclipse-wg/open-regulatory-compliance-wg/cra-topics#what-is-the-cra
