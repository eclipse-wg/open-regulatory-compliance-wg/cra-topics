# External resources related to the CRA

## General backgroud information

- [Good News on the Cyber Resilience Act](https://eclipse-foundation.blog/2023/12/19/good-news-on-the-cyber-resilience-act/)
- [What I learned in Brussels: the Cyber Resilience Act](https://blog.nlnetlabs.nl/what-i-learned-in-brussels-the-cyber-resilience-act/)
- [Understanding the Cyber Resilience Act: What Everyone involved in Open Source Development Should Know](https://www.linuxfoundation.org/blog/understanding-the-cyber-resilience-act)
- [EU CRA: What does it mean for open source?](https://berthub.eu/articles/posts/eu-cra-what-does-it-mean-for-open-source/)