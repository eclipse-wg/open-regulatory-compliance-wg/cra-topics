# EU Cyber Resilience Act (CRA) Discussion Topics

## What is the CRA?

The [EU Cyber Resilience Act (CRA)](https://digital-strategy.ec.europa.eu/en/policies/cyber-resilience-act) is an upcoming EU Regulation that aims to safeguard consumers and businesses who use software or products with a digital components. It creates mandatory cybersecurity requirements for manufacturers and retailers that extend throughout the product lifecycle and helps consumers and business identify such products through the CE mark.

### Main elements of the law

- Cybersecurity rules for [hardware and software](#scope) that is _placed on the market_
- Obligations for manufacturers, distributors, and importers
- Cybersecurity essential requirements across the life cycle
- Harmonised standards to follow
- [Conformity assessment](#conformity-assessment)
- Reporting obligations
- Market surveillance and enforcement

### Scope

- Hardware products (e.g. laptops, smart appliances, mobile phones, network equipment, CPUs, etc.)
- Software products (e.g. operating systems, word processing, games or mobile apps, software libraries, etc.)
- Remote data processing solutions for any of the above

### Conformity assessment

Verifying conformity assessment is different for each level of risk.

| Category | Examples | Assessment Type |
|----------|----------|-----------------|
| Open source | Web development frameworks, operating systems, database management systems, etc. | Self-assessment (unless categorized as "critical products") |
| Default | Memory chips, mobile apps, smart speakers, computer games, etc. | Self-assessment |
| Important products | Operating systems, anti-virus, routers, firewalls, etc. | Application of standards/3rd-party assessment | 
| Critical products | Smart cards, secure elements, smart meter gateways, etc. | Potentially certification in the future |

## CRA Resources

* [Final text of the CRA](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0130_EN.html) ([pdf](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0130_EN.pdf)|[docx](https://www.europarl.europa.eu/doceo/document/TA-9-2024-0130_EN.docx))
* [European Commission Draft standardisation request](https://ec.europa.eu/docsroom/documents/58974)
* [CRA Requirements Standards Mapping](https://www.enisa.europa.eu/publications/cyber-resilience-act-requirements-standards-mapping) - Joint Research Centre & ENISA analysis of existing standards.

## Discussion topics

### Active topics

* [Introduction to the Cyber Resilience Act](./topics/cra-intro/)
* [Definition of Critical and Important Product in the CRA](./topics/prod-defs/)

### Proposed topics

_Proposed discussion topics need the support of 5 people on the mailing list to get started. Any WG member can propose a discussion topic._

* [Open source stewards](./proposed-topics/open-source-stewards.md)

## WG Resources

- [Mailing List](https://accounts.eclipse.org/mailing-list/open-regulatory-compliance) ([Archive](https://www.eclipse.org/lists/open-regulatory-compliance/maillist.html))
- [Matrix Chat](https://chat.eclipse.org/#/room/#open-regulatory-compliance:matrix.eclipse.org)
- [Community Calendar](https://calendar.google.com/calendar/u/0/embed?src=c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea@group.calendar.google.com) ([iCal format](https://calendar.google.com/calendar/ical/c_7db8e3f13c4fac984103918a97c704bb1d619da0fdb66d33f1747849b6020aea%40group.calendar.google.com/public/basic.ics)) - meeting times of the discussion groups
- [Antitrust Policy](https://www.eclipse.org/org/documents/Eclipse_Antitrust_Policy.pdf)
- [Code of Conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php)

